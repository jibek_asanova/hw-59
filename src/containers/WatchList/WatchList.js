import React, {Component} from 'react';
import AddFilmForm from "../../components/AddFilmForm/AddFilmForm";
import Film from "../../components/Film/Film";

class WatchList extends Component {
    state = {
        films: [
            {name: 'Harry Potter', id: 1},
            {name: 'Titanic', id: 2},
            {name: 'Forrest Gump', id: 3},
        ],
        val: ''
    };

    onInputChange = e => {
        this.setState({...this.state, val: e.target.value});
    };

    addFilm = () => {
        if (this.state.val) {
            const films = [...this.state.films];
            films.push({name: this.state.val, id: Math.random()});

            this.setState({...this.state, films});
        }
    };

    removeFilm = id => {
        const films = [...this.state.films];
        const filteredFilms = films.filter(f => f.id !== id);
        this.setState({...this.state, films: filteredFilms});
    };

    changeFilmName = (id, name) => {
        this.setState({
            ...this.state, films: this.state.films.map(f => {
                if (f.id === id) {
                    return {...f, name};
                }
                return f;
            })
        })
    }

    filmComponents = this.state.films.map(film => (
        <Film
            key={film.id}
            id={film.id}
            name={film.name}
            onRemove={() => this.removeFilm(film.id)}>
        </Film>
    ))


    render() {
        return (
            <>
                <h1>Add film name to watch:</h1>
                <AddFilmForm
                    onInputChange={this.onInputChange}
                    val={this.state.val}
                    addFilm={this.addFilm}
                />
                {this.state.films.map(film => (
                    <Film
                        key={film.id}
                        id={film.id}
                        name={film.name}
                        onFilmChange={e => this.changeFilmName(film.id, e.target.value)}
                        onRemove={() => this.removeFilm(film.id)}>
                    </Film>
                ))}
            </>
        );
    }
}

export default WatchList;