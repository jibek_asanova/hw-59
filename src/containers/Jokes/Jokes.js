import React, {useEffect, useState} from 'react';
import Button from "../../components/Button/Button";

const url = 'https://api.chucknorris.io/jokes/random';
const Jokes = () => {
    const [jokes, setJokes] = useState([
        {value: ''}
    ]);
    const [page, setPage] = useState(1);

    useEffect(() => {
        const fetchData = async () => {
            const response = await fetch(url + '?_limit=10&_page=' + page);

            if(response.ok) {
                const jokes = await response.json();
                setJokes(jokes);
            }
        };

        fetchData().catch(e => console.error(e));
    }, [page]);

    return (
        <div>
            <h1>Jokes</h1>
            <p><strong>Joke: </strong>{jokes.value}</p>
            <Button buttonHandler={() => setPage(prev => prev +1)}/>
        </div>
    );
};

export default Jokes;