import React, {Component} from 'react';
import './Film.css';

class Film extends Component {
    shouldComponentUpdate(nextProps) {
        console.log(this.props.name, nextProps.name,'should update');

        return nextProps.name !== this.props.name
    }

    render() {
        console.log('should update jll');
        return (
            <div className="Film">
                <input
                    className="FilmInput"
                    type="text"
                    value={this.props.name}
                    onChange={this.props.onFilmChange}
                />
                <button onClick={this.props.onRemove} className="DeleteButton">Delete</button>
            </div>
        );
    }
}

export default Film;