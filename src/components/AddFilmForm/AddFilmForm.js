import React, {Component} from 'react';
import './AddFilmForm.css';

class AddFilmForm extends Component {
    render() {
        return (
            <div className='FilmForm'>
                <input
                    className="form"
                    type="text"
                    value={this.props.val}
                    onChange={this.props.onInputChange}
                />
                <button onClick={this.props.addFilm} className="FormButton">Add film</button>
            </div>
        );
    }
}

export default AddFilmForm;